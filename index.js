// CONCEITO
// let imagem = downloadImagem(urlImagem)
// console.log(imagem)
// undefined

// downloadImagem(urlImagem)
// .then(response => console.log(response))
// .catch(error => console.warn(error))

// PRÁTICA DE AJAX

// fetch("https://treinamentoajax.herokuapp.com/messages/27")
// .then(response => response.json())
// .then(response => console.log(response))

//POST

// const fetchBody = {
//     "message":{
//         "name": "Rafael Carrilho",
//         "message": "Testando pelo JS"
//     }
// }
// const fetchConfig = {
//     method: "POST",
//     headers: {"Content-Type":"application/JSON"},
//     body: JSON.stringify(fetchBody)
// }

// fetch("https://treinamentoajax.herokuapp.com/messages", fetchConfig)
// .then(console.log)

//DELETE

// fetch("https://treinamentoajax.herokuapp.com/messages/19", {
//     method:"DELETE"
// }).then(console.log)

//PUT/PATCH

let fetchBody={
    "message":{
        "message":"qualquer coisa"
    }
}

let fetchConfig={
    method:"PUT",
    headers:{"Content-Type": "application/JSON"},
    body: JSON.stringify(fetchBody)
}

fetch("https://treinamentoajax.herokuapp.com/messages/21", fetchConfig)
.then(response => response.json())
.then(console.log)
